# Fake API Proxy


NGINX proxy for API

## Usage

### Env variables

* `LISTEN_PORT` - listening port (default: `8000`)
* `APP_HOST` - hostname for application to forward requests to (default: `app`)
* `APP_PORT` - Port of application to forward requests to (default: `9000`)
